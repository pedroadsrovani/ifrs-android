/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    // TODO (1) Crie uma variável privada static final int chamada NUM_LIST_ITEMS e dê o valor 100

    // TODO (2) Crie uma variável do tipo GreenAdapter chamada mAdapter
    // TODO (3) Crie uma variável do tipo RecyclerView chamada mNumbersList

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO (4) Use findViewById para armazenar uma referência para mNumbersList

        // TODO (5) Crie uma variável do tipo LinearLayoutManager chamada layoutManager
        // TODO (6) Use setLayoutManager para configurar o LinearLayoutManager de mNumbersList

        // TODO (7) Chame setHasFixedSize(true) para não permitir que o conteúdo do RecyclerView mude de tamanho

        // TODO (8) Armazene um novo GreenAdapter na variável mAdapter e passe NUM_LIST_ITEMS

        // TODO (9) Configure o adaptador GreenAdapter para mNumbersList
    }
}
